import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})

export class GameComponent implements OnInit {

  /*
  *---BOARD LAYOUT KEY---
  *The 3x3 Tic-tac-toe game board is represented as follows:
  *
  * |0|1|2|
  * |3|4|5|
  * |6|7|8|
  *
  *Each numbered location on the board maps to the corresponding index in the gameBoard array.
  *The array value at the index is 0 if the square is available to a player, 1 if it is an 'X', or 2 if it is an 'O'.
  *Terminology: Xs are also known as draughts. Os are also known as naughts.
   */

  gameBoard: number[] = new Array(9);

  // Arrays with indexes (from gameBoard) of all possible winning combinations, 8 in all.
  // A player with Xs or Os at indices matching one of the arrays wins the game.
  readonly winningCombos = [
    [0, 1, 2], // Row 1
    [3, 4, 5], // Row 2
    [6, 7, 8], // Row 3
    [0, 3, 6], // Column 1
    [1, 4, 7], // Column 2
    [2, 5, 8], // Column 3
    [0, 4, 8], // Diagonal 1
    [2, 4, 6] // Diagonal 2
  ];

  // When a win occurs, the indices (squares) are recorded. This facilitates highlighting of winning squares.
  winningIndices = new Set<number>();

  // A won or drawn position triggers the end of play.
  wonPosition = false;
  drawnPosition = false;

  // The active player is either the Xs (draughts) player or the Os (noughts) player.
  xPlayerActive: boolean = true;

  // A basic summary of the state of the game and a CSS class name to color the message.
  announcement = '';
  announcementColorClass = 'draughts';   // Options: draughts, naughts, won, drawn

  ngOnInit(): void {
    // If a square (array element) is zero, it is available to a player on their turn to place an 'X' or 'O'.
    this.gameBoard.fill(0);
    this.setAnnouncement();
  }

  // Clear the game state to set up a new game.
  resetGame(): void {
    this.gameBoard.fill(0);
    // The Xs (draughts) player is always first to play in this implementation.
    this.xPlayerActive = true;
    this.winningIndices.clear();
    this.wonPosition = false;
    this.drawnPosition = false;
    this.setAnnouncement();
  }

  // User has clicked a square.
  squareClicked(index: number): void {
    if (this.isLegalMove(index)) {
      // Record 'X' or 'O' move based on active player.
      this.xPlayerActive ? this.gameBoard[index] = 1 : this.gameBoard[index] = 2;

      // Check the board to see if the game is a win or draw.
      this.checkGameState();

      // Switch players if no win or draw detected.
      if (!this.wonPosition && !this.drawnPosition) {
        this.xPlayerActive ? this.xPlayerActive = false : this.xPlayerActive = true;
      }
      // Update announcement.
      this.setAnnouncement();
    }
  }

  // A move is legal if a square is empty (value of 0 in gameBoard) and position is not won or drawn.
  isLegalMove(index: number): boolean {
    return this.gameBoard[index] === 0 && !this.wonPosition && !this.drawnPosition;
  }

  // Check board position for win or draw and record findings.
  checkGameState(): void {
    this.winningIndices = this.winningComboCheck();
    if (this.winningIndices.size > 0) {
      this.wonPosition = true;
    } else {
      this.drawnPosition = this.isPositionDrawn();
    }
  }

  // Returns a set with gameBoard indices from a winning combo or combos.
  winningComboCheck(): Set<number> {
    const winningIndices = new Set<number>();
    let player: number;
    this.xPlayerActive ? player = 1 : player = 2;
    for (let combo of this.winningCombos) {
      if (this.gameBoard[combo[0]] === player &&
        this.gameBoard[combo[1]] === player &&
        this.gameBoard[combo[2]] == player) {
        combo.forEach(index => winningIndices.add(index));
      }
    }
    return winningIndices;
  }

  // Identifies a drawn position on the board.
  isPositionDrawn(): boolean {
    for (let square of this.gameBoard) {
      if (square === 0 && !this.wonPosition) {
        return false;
      }
    }
    return true;
  }

  // Sets message on state of game (win or draw) or indicates which player is to move. Message color is also handled.
  setAnnouncement(): void {
    if (this.wonPosition) {
      this.announcement = this.xPlayerActive ? 'X player wins!' : 'O player wins!';
      this.announcementColorClass = 'won';
    } else if (this.drawnPosition) {
      this.announcement = 'The game is drawn!';
      this.announcementColorClass = 'drawn';
    } else {
      this.xPlayerActive ? this.announcement = 'X player to move!' : this.announcement = 'O player to move!';
      this.xPlayerActive ? this.announcementColorClass = 'draughts' : this.announcementColorClass = 'naughts';
    }
  }
}
