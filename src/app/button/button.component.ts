import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})

// This component abstracts buttons.
// If the app were to be expanded, this component makes it quick and easy to give buttons a consistent look and feel.
export class ButtonComponent {
  @Input() type: string = '';
  @Input() text: string = '';
  @Output() btnClick = new EventEmitter();


  onClick() {
    this.btnClick.emit();
  }

}
